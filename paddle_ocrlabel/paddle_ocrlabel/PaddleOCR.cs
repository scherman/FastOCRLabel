﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paddle_ocrlabel
{
    public class PaddleOCR
    {
        public List<PaddleOCRForPoly> paddle_result { get; set; }
    }

    public class PaddleOCRForPoly
    {
        public List<PointF> points { get; set; }

        public bool isSelected { get; set; }
        public bool isNew { get; set; }

        public String text { get; set; }
        public int accuracy { get; set; }

        public override string ToString()
        {
            String result = "";
            foreach (var point in points)
            {
                result += String.Format("({0},{1}),", point.X, point.Y);
            }

            return "[" + result.Substring(0, result.Length - 1) + "]";
        }
    }

    public class PaddleOCRForOutput
    {
        public List<PaddleOCREntityForOutput> paddle_result { get; set; }

        public override string ToString()
        {
            String result = "";
            foreach (var entity in paddle_result)
            {
                result += entity.ToString() + ",";
            }

            return "[" + result.Substring(0, result.Length - 1) + "]";
        }
    }

    public class PaddleOCREntityForOutput
    {
        public List<Point> points { get; set; }

        public String transcription { get; set; }

        public override string ToString()
        {
            String result = "";
            foreach (var point in points)
            {
                result += String.Format("[{0},{1}],", point.X, point.Y);
            }

            result = "[" + result.Substring(0, result.Length - 1) + "]";

            return "{\"transcription\": \"" + transcription + "\", \"points\":" + result + "}";
        }
    }
}
