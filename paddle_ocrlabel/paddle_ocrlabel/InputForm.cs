﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paddle_ocrlabel
{
    public partial class InputForm : Form
    {
        public InputForm()
        {
            InitializeComponent();
        }       

        public Form5 form5 { get; set; }
        public string originalText { get; set; }

        public String outputText { get; set; }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            this.outputText = txtContent.Text.Trim();
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void InputForm_Load(object sender, EventArgs e)
        {
            txtContent.Text = originalText;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            int WM_KEYDOWN = 256;
            int WM_SYSKEYDOWN = 260;
            if (msg.Msg == WM_KEYDOWN | msg.Msg == WM_SYSKEYDOWN)
            {
                switch (keyData)
                {
                    case Keys.Escape:
                        DialogResult = DialogResult.Cancel;
                        break;
                }

            }
            return false;
        }
    }
}