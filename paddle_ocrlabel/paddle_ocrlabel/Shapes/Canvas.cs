﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Drawing.Drawing2D;

namespace paddle_ocrlabel
{
    public class Canvas
    {
        public Canvas()
        {
            Shapes = new List<Shape>();
        }

        public List<Shape> Shapes { get; set; }

        public Shape GetShape(int x, int y)
        {
            for (int i = Shapes.Count - 1; i >= 0; --i)
                if (Shapes[i].In(x, y))
                    return Shapes[i];
            return null;
        }

        public void ClearSelection()
        {
            foreach (Shape s in Shapes)
                s.Selected = false;
        }

        public Mark GetMark(int x, int y, double scale)
        {
            foreach (Shape s in Shapes)
                if (s.Selected)
                    for (int i = s.Marks.Count - 1; i >= 0; i--)
                    {
                        Mark m = s.Marks[i];
                        if (m.In(x, y, s.points[m.Pos], scale))
                        {
                            return m;
                        }
                    }
            return null;
        }
    }
}
