﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace paddle_ocrlabel
{
    public class Mark
    {
        public Mark(Shape shape, Cursor cursor)
        {
            this.shape = shape;
            this.cursor = cursor;
        }

        private Shape shape;

        private Cursor cursor;

        public Cursor Cursor
        {
            get { return cursor; }
            set { cursor = value; }
        }

        public Shape Shape
        {
            get { return shape; }
        }

        public void MoveTo(int x, int y, PointF duijiaoPoint)
        {
            if (shape != null)
            {
                if (shape.isRec())
                {
                    shape.MoveThreePointsTo(x, y, duijiaoPoint);

                }
                else
                {
                    shape.MoveOnePointTo(Pos, x, y);
                }
            }
        }

        private int markSize = 8;

        public int MarkSize
        {
            get { return markSize; }
            protected set { markSize = value; }
        }

        public bool In(int x, int y, PointF corner, double scale)
        {
            if (Math.Abs(x - corner.X) * scale < markSize  &&
                Math.Abs(y - corner.Y) * scale < markSize)
                return true;
            return false;
        }

        public int Pos { get; set; }
    }
}
