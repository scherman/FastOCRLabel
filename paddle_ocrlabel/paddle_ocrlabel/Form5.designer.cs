﻿namespace paddle_ocrlabel
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu9 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOpenImageFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.menu5 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu51 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu11 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu12 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu14 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu13 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu6 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.picPreview = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lbImages = new System.Windows.Forms.ListBox();
            this.btnSingleLabel = new System.Windows.Forms.Button();
            this.rboDetect = new System.Windows.Forms.RadioButton();
            this.rboRec = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAutoLabel = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOCRAgain = new System.Windows.Forms.Button();
            this.btnRectLabel = new System.Windows.Forms.Button();
            this.tvResults = new System.Windows.Forms.TreeView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnFourPoints = new System.Windows.Forms.Button();
            this.rboFromRight = new System.Windows.Forms.RadioButton();
            this.rboFromTop = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnRollBack = new System.Windows.Forms.Button();
            this.btnExportBiaozhu = new System.Windows.Forms.Button();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu9,
            this.menu5,
            this.menu1,
            this.menu6});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1004, 25);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "整理书籍目录";
            // 
            // menu9
            // 
            this.menu9.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOpenImageFolder});
            this.menu9.Name = "menu9";
            this.menu9.Size = new System.Drawing.Size(44, 21);
            this.menu9.Text = "文件";
            // 
            // menuOpenImageFolder
            // 
            this.menuOpenImageFolder.Name = "menuOpenImageFolder";
            this.menuOpenImageFolder.Size = new System.Drawing.Size(180, 22);
            this.menuOpenImageFolder.Text = "打开目录";
            this.menuOpenImageFolder.Click += new System.EventHandler(this.menuOpenImageFolder_Click);
            // 
            // menu5
            // 
            this.menu5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu51});
            this.menu5.Name = "menu5";
            this.menu5.Size = new System.Drawing.Size(44, 21);
            this.menu5.Text = "设置";
            // 
            // menu51
            // 
            this.menu51.Name = "menu51";
            this.menu51.Size = new System.Drawing.Size(124, 22);
            this.menu51.Text = "设置代理";
            this.menu51.Click += new System.EventHandler(this.menu51_Click);            
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Controls.Add(this.picPreview);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(243, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(451, 664);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "标注预览";
            // 
            // picPreview
            // 
            this.picPreview.BackColor = System.Drawing.SystemColors.Window;
            this.picPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picPreview.Location = new System.Drawing.Point(3, 17);
            this.picPreview.Name = "picPreview";
            this.picPreview.Size = new System.Drawing.Size(445, 632);
            this.picPreview.TabIndex = 1;
            this.picPreview.TabStop = false;
            this.picPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.picPreview_Paint);
            this.picPreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picPreview_MouseDown);
            this.picPreview.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picPreview_MouseMove);
            this.picPreview.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picPreview_MouseUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label4.Location = new System.Drawing.Point(3, 649);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 12);
            this.label4.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 689);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1004, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbImages
            // 
            this.lbImages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbImages.FormattingEnabled = true;
            this.lbImages.ItemHeight = 12;
            this.lbImages.Location = new System.Drawing.Point(3, 148);
            this.lbImages.Name = "lbImages";
            this.lbImages.Size = new System.Drawing.Size(237, 520);
            this.lbImages.TabIndex = 29;
            this.lbImages.MouseCaptureChanged += new System.EventHandler(this.lbImages_MouseCaptureChanged);
            // 
            // btnSingleLabel
            // 
            this.btnSingleLabel.Location = new System.Drawing.Point(80, 86);
            this.btnSingleLabel.Name = "btnSingleLabel";
            this.btnSingleLabel.Size = new System.Drawing.Size(75, 23);
            this.btnSingleLabel.TabIndex = 32;
            this.btnSingleLabel.Text = "单张标注";
            this.btnSingleLabel.UseVisualStyleBackColor = true;
            this.btnSingleLabel.Click += new System.EventHandler(this.btnSingleLabel_Click);
            // 
            // rboDetect
            // 
            this.rboDetect.AutoSize = true;
            this.rboDetect.Location = new System.Drawing.Point(12, 60);
            this.rboDetect.Name = "rboDetect";
            this.rboDetect.Size = new System.Drawing.Size(107, 16);
            this.rboDetect.TabIndex = 33;
            this.rboDetect.Text = "只检测文字区域";
            this.rboDetect.UseVisualStyleBackColor = true;
            this.rboDetect.CheckedChanged += new System.EventHandler(this.rboDetect_CheckedChanged);
            // 
            // rboRec
            // 
            this.rboRec.AutoSize = true;
            this.rboRec.Checked = true;
            this.rboRec.Location = new System.Drawing.Point(12, 30);
            this.rboRec.Name = "rboRec";
            this.rboRec.Size = new System.Drawing.Size(143, 16);
            this.rboRec.TabIndex = 34;
            this.rboRec.TabStop = true;
            this.rboRec.Text = "检测区域，并识别文字";
            this.rboRec.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.btnExportBiaozhu);
            this.groupBox1.Controls.Add(this.btnAutoLabel);
            this.groupBox1.Controls.Add(this.rboRec);
            this.groupBox1.Controls.Add(this.rboDetect);
            this.groupBox1.Controls.Add(this.btnSingleLabel);
            this.groupBox1.Controls.Add(this.lbImages);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 664);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "文件列表";
            // 
            // btnAutoLabel
            // 
            this.btnAutoLabel.Location = new System.Drawing.Point(161, 86);
            this.btnAutoLabel.Name = "btnAutoLabel";
            this.btnAutoLabel.Size = new System.Drawing.Size(75, 23);
            this.btnAutoLabel.TabIndex = 35;
            this.btnAutoLabel.Text = "全部标注";
            this.btnAutoLabel.UseVisualStyleBackColor = true;
            this.btnAutoLabel.Click += new System.EventHandler(this.btnAutoLabel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "识别结果";
            // 
            // btnOCRAgain
            // 
            this.btnOCRAgain.Location = new System.Drawing.Point(122, 57);
            this.btnOCRAgain.Name = "btnOCRAgain";
            this.btnOCRAgain.Size = new System.Drawing.Size(114, 23);
            this.btnOCRAgain.TabIndex = 33;
            this.btnOCRAgain.Text = "重新识别选中区域";
            this.btnOCRAgain.UseVisualStyleBackColor = true;
            this.btnOCRAgain.Click += new System.EventHandler(this.btnOCRAgain_Click);
            // 
            // btnRectLabel
            // 
            this.btnRectLabel.Location = new System.Drawing.Point(6, 27);
            this.btnRectLabel.Name = "btnRectLabel";
            this.btnRectLabel.Size = new System.Drawing.Size(98, 23);
            this.btnRectLabel.TabIndex = 32;
            this.btnRectLabel.Tag = "1";
            this.btnRectLabel.Text = "启动矩形标注";
            this.btnRectLabel.UseVisualStyleBackColor = true;
            this.btnRectLabel.Click += new System.EventHandler(this.btnRectLabel_Click);
            // 
            // tvResults
            // 
            this.tvResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvResults.HideSelection = false;
            this.tvResults.Location = new System.Drawing.Point(3, 214);
            this.tvResults.Name = "tvResults";
            this.tvResults.Size = new System.Drawing.Size(304, 456);
            this.tvResults.TabIndex = 120;
            this.tvResults.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvResults_NodeMouseClick);
            this.tvResults.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvResults_NodeMouseDoubleClick);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Controls.Add(this.btnFourPoints);
            this.groupBox3.Controls.Add(this.rboFromRight);
            this.groupBox3.Controls.Add(this.rboFromTop);
            this.groupBox3.Controls.Add(this.btnSave);
            this.groupBox3.Controls.Add(this.btnRollBack);
            this.groupBox3.Controls.Add(this.tvResults);
            this.groupBox3.Controls.Add(this.btnRectLabel);
            this.groupBox3.Controls.Add(this.btnOCRAgain);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox3.Location = new System.Drawing.Point(694, 25);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(310, 664);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "工具箱";
            // 
            // btnFourPoints
            // 
            this.btnFourPoints.Location = new System.Drawing.Point(122, 27);
            this.btnFourPoints.Name = "btnFourPoints";
            this.btnFourPoints.Size = new System.Drawing.Size(98, 23);
            this.btnFourPoints.TabIndex = 126;
            this.btnFourPoints.Tag = "1";
            this.btnFourPoints.Text = "启动四点标注";
            this.btnFourPoints.UseVisualStyleBackColor = true;
            this.btnFourPoints.Click += new System.EventHandler(this.btnFourPoints_Click);
            // 
            // rboFromRight
            // 
            this.rboFromRight.AutoSize = true;
            this.rboFromRight.Location = new System.Drawing.Point(9, 180);
            this.rboFromRight.Name = "rboFromRight";
            this.rboFromRight.Size = new System.Drawing.Size(179, 16);
            this.rboFromRight.TabIndex = 124;
            this.rboFromRight.Text = "竖排版：从右到左，自上而下";
            this.rboFromRight.UseVisualStyleBackColor = true;
            // 
            // rboFromTop
            // 
            this.rboFromTop.AutoSize = true;
            this.rboFromTop.Checked = true;
            this.rboFromTop.Location = new System.Drawing.Point(9, 147);
            this.rboFromTop.Name = "rboFromTop";
            this.rboFromTop.Size = new System.Drawing.Size(179, 16);
            this.rboFromTop.TabIndex = 123;
            this.rboFromTop.TabStop = true;
            this.rboFromTop.Text = "横排版：自上而下，从左到右";
            this.rboFromTop.UseVisualStyleBackColor = true;
            this.rboFromTop.CheckedChanged += new System.EventHandler(this.rboFromTop_CheckedChanged);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(6, 86);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 23);
            this.btnSave.TabIndex = 121;
            this.btnSave.Tag = "1";
            this.btnSave.Text = "保存标注(Ctrl+S)";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRollBack
            // 
            this.btnRollBack.Location = new System.Drawing.Point(122, 86);
            this.btnRollBack.Name = "btnRollBack";
            this.btnRollBack.Size = new System.Drawing.Size(114, 23);
            this.btnRollBack.TabIndex = 122;
            this.btnRollBack.Text = "撤销标注(Ctrl+Z)";
            this.btnRollBack.UseVisualStyleBackColor = true;
            this.btnRollBack.Click += new System.EventHandler(this.btnRollBack_Click);
            // 
            // btnExportBiaozhu
            // 
            this.btnExportBiaozhu.Location = new System.Drawing.Point(80, 121);
            this.btnExportBiaozhu.Name = "btnExportBiaozhu";
            this.btnExportBiaozhu.Size = new System.Drawing.Size(156, 23);
            this.btnExportBiaozhu.TabIndex = 38;
            this.btnExportBiaozhu.Text = "导出该目录下所有标注";
            this.btnExportBiaozhu.UseVisualStyleBackColor = true;
            this.btnExportBiaozhu.Click += new System.EventHandler(this.btnExportBiaozhu_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(1004, 711);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(1020, 726);
            this.Name = "Form5";
            this.Text = "OCR自动标注";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form5_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form5_KeyDown);
            this.Resize += new System.EventHandler(this.Form5_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu1;
        private System.Windows.Forms.ToolStripMenuItem menu11;
        private System.Windows.Forms.ToolStripMenuItem menu12;
        private System.Windows.Forms.ToolStripMenuItem menu13;
        private System.Windows.Forms.ToolStripMenuItem menu14;
        private System.Windows.Forms.ToolStripMenuItem menu5;
        private System.Windows.Forms.ToolStripMenuItem menu6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStripMenuItem menu51;
        private System.Windows.Forms.ListBox lbImages;
        private System.Windows.Forms.Button btnSingleLabel;
        private System.Windows.Forms.RadioButton rboDetect;
        private System.Windows.Forms.RadioButton rboRec;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOCRAgain;
        private System.Windows.Forms.Button btnRectLabel;
        private System.Windows.Forms.TreeView tvResults;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRollBack;
        private System.Windows.Forms.RadioButton rboFromRight;
        private System.Windows.Forms.RadioButton rboFromTop;
        private System.Windows.Forms.Button btnAutoLabel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.PictureBox picPreview;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem menu9;
        private System.Windows.Forms.ToolStripMenuItem menuOpenImageFolder;
        private System.Windows.Forms.Button btnFourPoints;
        private System.Windows.Forms.Button btnExportBiaozhu;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}