﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paddle_ocrlabel
{
    public class Singleton
    {
        private static Singleton mySingleton = null;

        private Singleton()
        {

        }

        public static Singleton Instance()
        {
            if (mySingleton == null)
                mySingleton = new Singleton();
            return mySingleton;
        }

        public void Do()
        {
            //Do something
        }

        public String pre_url = @"http://127.0.0.1:5000/";        
    }
}
