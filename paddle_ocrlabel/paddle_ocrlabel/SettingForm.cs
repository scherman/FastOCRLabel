﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paddle_ocrlabel
{
    public partial class SettingForm : Form
    {
        public SettingForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var cacheProxyFilePath = System.Environment.CurrentDirectory + @"\Cache\proxy.txt";
            using (StreamWriter rewriter = new StreamWriter(cacheProxyFilePath, false, Encoding.UTF8))
            {
                var content = tbRestHost.Text.Trim();
                if(!content.EndsWith("/"))
                {
                    content = content + @"/";
                }

                rewriter.WriteLine(content);
            }

            Singleton.Instance().pre_url = tbRestHost.Text.Trim();

            DialogResult = DialogResult.OK;
        }

        private void SettingForm_Load(object sender, EventArgs e)
        {
            var cacheProxyFilePath = System.Environment.CurrentDirectory + @"\Cache\proxy.txt";
            if (File.Exists(cacheProxyFilePath))
            {
                String content = null;
                using (StreamReader reader = new StreamReader(cacheProxyFilePath, Encoding.UTF8))
                {
                    content = reader.ReadLine();
                }

                if (content.Length > 0)
                {
                    if (!content.EndsWith("/"))
                    {
                        content = content + @"/";
                    }

                    tbRestHost.Text = content;
                }
                else
                {
                    tbRestHost.Text = Singleton.Instance().pre_url;
                }
            }
            else
            {
                tbRestHost.Text = Singleton.Instance().pre_url;
            }
        }
    }
}