﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paddle_ocrlabel
{
    public class Command
    {
        public CommandEnum type { get; set; }
        public PaddleOCRForPoly oldPoly;
        public PaddleOCRForPoly newPoly;
    }

    public enum CommandEnum
    {
        Add,
        Delete,
        ModifyText,
        Move,
    }
}
