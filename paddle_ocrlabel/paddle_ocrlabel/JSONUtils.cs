﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paddle_ocrlabel
{
    class JSONUtils
    {
        public static string Obj2Json(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        public static T Json2Obj<T>(string s)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(s);
            }
            catch (Exception ex)
            {
                return default(T);
            }

        }
    }
}
