﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paddle_ocrlabel
{
    class FileUtils
    {
        public static void writeFileContent(String filePath, String content)
        {
            FileStream fs = new FileStream(filePath, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
            sw.WriteLine(content);
            sw.Close();
            fs.Close();
        }

        public static String readFileContent(String dataFilePath)
        {
            FileStream fs = new FileStream(dataFilePath, FileMode.Open);
            StreamReader sr = new StreamReader(fs, Encoding.UTF8);
            var tempString = sr.ReadToEnd();
            sr.Close();
            fs.Close();

            return tempString;
        }

        //判断字符串中相同字符串的个数
        public static int SubstringCount(string str, string substring)
        {
            if (str.Contains(substring))
            {
                string strReplaced = str.Replace(substring, "");
                return (str.Length - strReplaced.Length) / substring.Length;
            }

            return 0;
        }

        public static void createDirectory(String path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static List<String> getDirectoryOfPath(String path)
        {
            List<String> result = new List<string>();
            
            if (path.EndsWith("\\"))
            {
                path = path.Substring(0, path.Length - 1);
            }

            var arr = path.Split('\\');
            result.Add(arr[arr.Length - 1]);
            result.Add(arr[arr.Length - 2]);

            return result;
        }

        public static String SwitchDirectory(String path, String newDirectory)
        {
            if (path.EndsWith("\\"))
            {
                path = path.Substring(0, path.Length - 1);
            }

            var arr = path.Split('\\');
            arr[arr.Length - 1] = newDirectory;

            return String.Join(@"\", arr);
        }

        public static String getParentPath(String path)
        {
            if (path.EndsWith("\\"))
            {
                path = path.Substring(0, path.Length - 1);
            }

            var arr = path.Split('\\');
            String lastDirectory = arr[arr.Length - 1];

            int pos2 = path.LastIndexOf(lastDirectory);
            return path.Substring(0, pos2);
        }

        public static List<String> getFileNameOfPath(String filePath)
        {
            List<String> result = new List<string>();

            var arr = filePath.Split('\\');
            var fullName = arr[arr.Length - 1];
            result.Add(fullName);

            var arr2 = fullName.Split('.');
            result.Add(arr2[0]);
            result.Add(arr2[1]);

            return result;
        }

        public static string UrlEncode(string str)
        {
            StringBuilder sb = new StringBuilder();
            byte[] byStr = System.Text.Encoding.UTF8.GetBytes(str); //默认是System.Text.Encoding.Default.GetBytes(str)
            for (int i = 0; i < byStr.Length; i++)
            {
                sb.Append(@"%" + Convert.ToString(byStr[i], 16));
            }

            return (sb.ToString());
        }
    }
}
